# This is my package eloquent-defaultattributes

[![Latest Version on Packagist](https://img.shields.io/packagist/v/kda/eloquent-defaultattributes.svg?style=flat-square)](https://packagist.org/packages/kda/eloquent-defaultattributes)
[![Total Downloads](https://img.shields.io/packagist/dt/kda/eloquent-defaultattributes.svg?style=flat-square)](https://packagist.org/packages/kda/eloquent-defaultattributes)

## Installation

You can install the package via composer:

```bash
composer require kda/eloquent-defaultattributes
```
