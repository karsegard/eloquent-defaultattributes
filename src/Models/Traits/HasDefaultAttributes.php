<?php

namespace KDA\Eloquent\DefaultAttributes\Models\Traits;

use Illuminate\Database\Eloquent\Model;

trait HasDefaultAttributes
{

    public static function bootHasDefaultAttributes(): void
    {
        static::creating(function ($model) {
            if (method_exists($model, 'createDefaultAttributes')) {
                $model->createDefaultAttributes();
            }
            if (method_exists($model, 'applyDefaultAttributes')) {
                $model->applyDefaultAttributes();
            }
        });
        static::created(function ($model) {
          
            if (method_exists($model, 'createdDefaultAttributes')) {
                $model->createdDefaultAttributes();
            }
            if (method_exists($model, 'applyDefaultAttributes')) {
                $model->applyDefaultAttributes();
            }
        });
        static::updating(function ($model) {
            if (method_exists($model, 'updateDefaultAttributes')) {
                $model->updateDefaultAttributes();
            }
            if (method_exists($model, 'applyDefaultAttributes')) {
                $model->applyDefaultAttributes();
            }
        });

        static::updated(function ($model) {
          
            if (method_exists($model, 'updatedDefaultAttributes')) {
                $model->updatedDefaultAttributes();
            }
            if (method_exists($model, 'applyDefaultAttributes')) {
                $model->applyDefaultAttributes();
            }
        });
    }

    public function defaultAttribute($name, mixed $value)
    {
        if(!isset($this->attributes[$name]) || blank($this->attributes[$name])){
            if(is_callable($value)){
                $value = $value($this,$name);
            }
            $this->attributes[$name]= $value;
        }
    }

    public function defaultMorphAttribute($morph,Model $value){
        $this->defaultAttribute($morph.'_id',$value->getKey());
        $this->defaultAttribute($morph.'_type',get_class($value));
    }
/*
    public function has_many_polymorphic()
    {
        return $this->morphMany(Slug::class, 'sluggable');
    }

    public function has_one_of_many_polymorphic()
    {
        return $this->morphOne(Slug::class, 'sluggable')->latestOfMany();
    }

    public function scopeWithNoSlug($query)
    {
        return $query->whereDoesntHave('slugs');
     }*/
}
