<?php

namespace KDA\Tests\Unit;

use Illuminate\Foundation\Testing\RefreshDatabase;
use KDA\Sluggable\Models\SlugCollection;

use KDA\Tests\TestCase;

class SlugCollectionTest extends TestCase
{
  use RefreshDatabase;

  
  /** @test */
  function a_slugcollection_has_a_slug()
  {
    $o = SlugCollection::factory()->create(['slug' => 'Fake Title']);
    $this->assertEquals('Fake Title', $o->slug);

  }


  /** @test */
  
  function a_slugcollection_has_a_name_and_slug()
  {
    $o = SlugCollection::factory()->create(['name' => 'Fake Title']);
    $this->assertEquals('fake_title', $o->slug);

  }

  
}